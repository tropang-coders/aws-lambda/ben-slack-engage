'use strict';

const {
  SecretsManagerClient,
  GetSecretValueCommand,
} = require('@aws-sdk/client-secrets-manager');

async function getSecrets() {
  const secret_name = 'ben-slack-engage';
  const client = new SecretsManagerClient({
    region: 'us-east-1',
  });

  let response;

  try {
    response = await client.send(
      new GetSecretValueCommand({
        SecretId: secret_name,
        VersionStage: 'AWSCURRENT',
      })
    );
  } catch (err) {
    console.error(err);
  }

  console.log(`Response`);
  console.log(response);
}

const slackConfig = {
  SLACK_SIGNING_SECRET: process.env.SLACK_SIGNING_SECRET,
  SLACK_BOT_TOKEN: process.env.SLACK_BOT_TOKEN,
};

const snowConfig = {
  SNOW_BASE_URL: process.env.SNOW_BASE_URL,
  SNOW_API_KEY: process.env.SNOW_API_KEY,
};

const sidConfig = {
  SID_BASE_URL: process.env.SID_BASE_URL,
  SID_TOKEN: process.env.SID_TOKEN,
};

const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_ID,
};

module.exports = {
  firebaseConfig,
  slackConfig,
  snowConfig,
  sidConfig,
  firebaseConfig,
};
