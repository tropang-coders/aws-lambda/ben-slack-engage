'use strict'
const { awsLambdaReceiver } = require('./lib/slack-bolt/bolt-config');
require('./lib/slack-bolt/engage-handler');

exports.lambdaHandler = async (event, context, callback) => {
    const handler = await awsLambdaReceiver.start();
    return handler(event, context, callback);
};
