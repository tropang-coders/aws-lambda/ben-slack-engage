const { initializeApp } = require('firebase/app');
const { getDatabase, ref, child, get } = require('firebase/database');
const { firebaseConfig } = require('../../app-config');

async function getActiveBridges() {
  const app = initializeApp(firebaseConfig);
  const dbRef = ref(getDatabase());
  const activeBridges = await get(child(dbRef, 'active_bridges'));
  console.log(activeBridges.val());
}

module.exports = {
    getActiveBridges
}
