const axios = require('axios');
const { sidConfig } = require('../../app-config');

const BASE_URL = `${sidConfig.SID_BASE_URL}/sidsvc/application/v1`;

const headers = {
    'Content-Type': 'application/json',
    'x-api-key': sidConfig.SID_TOKEN,
    'Accept': 'application/json'
}

const sidClient = axios.create({
    baseURL: BASE_URL,
    headers
});

async function getIncident(number) {
    return sidClient.get(`/incident/${number}`);
}

module.exports = {
    getIncident
}