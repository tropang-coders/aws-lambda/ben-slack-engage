'use strict'

const axios = require('axios');
const { snowConfig } = require('../../app-config');

const BASE_URL = `${snowConfig.SNOW_BASE_URL}/api/now/v2/table`

const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Basic ${snowConfig.SNOW_API_KEY}`
}

const snowClient = axios.create({
    baseURL: BASE_URL,
    headers
})

const SNOW_USER_TABLE = 'sys_user'

async function searchUser(username) {
    let res = [];
    const encodedName = encodeURIComponent(username);
    
    const query = `sysparm_query=nameLIKE${encodedName}%5EemailISNOTEMPTY&sysparm_fields=manager.sys_id,sys_id,manager,name,company,u_org_unit,title,email&sysparm_limit=20&sysparm_offset=0&sysparm_display_value=true`;

    try {
        console.log(`whole query: ${BASE_URL}/${SNOW_USER_TABLE}?${query}`)
        res = await snowClient.get(`/${SNOW_USER_TABLE}?${query}`)
    } catch(err) {
        throw new Error(`Fatal: Failed to search user. ${err}`);
    }

    return res;
}

module.exports = {
    searchUser
}