'use strict';

const { app } = require('./bolt-config');
const { searchUser } = require('../api/snow-api');
const { getActiveBridges } = require('../api/firebase-api');

const initianlFormView = {
  type: 'modal',
  callback_id: 'engage_name',
  title: {
    type: 'plain_text',
    text: 'Search user to engage',
  },
  blocks: [
    {
      type: 'divider',
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'User',
      },
      accessory: {
        type: 'users_select',
        placeholder: {
          type: 'plain_text',
          text: 'Select a user',
          emoji: true,
        },
        action_id: 'users_select-action',
      },
    },
  ],
};

function snowUserFoundBlock(user) {
  return {
    type: 'modal',
    callback_id: 'engage_name',
    title: {
      type: 'plain_text',
      text: 'Engage by name',
    },
    blocks: [
      {
        type: 'divider',
      },
      {
        type: 'input',
        block_id: 'message_input_block',
        element: {
          type: 'plain_text_input',
          multiline: true,
          action_id: 'message_input',
        },
        label: {
          type: 'plain_text',
          text: `User Found: ${user}`,
        },
      },
    ],
    submit: {
      type: 'plain_text',
      text: 'Engage',
    },
  };
}

app.command('/engage', async ({ ack, body, client, command, logger }) => {
  await ack(); //aknowledge the command request
  console.log(`You have triggered slack command "\\engage"`);
  console.log(`Command: `);
  console.log(command);
  console.log(`ChannelId: ${command.channel_id}`);
  try {
    const res = await client.views.open({
      trigger_id: body.trigger_id,
      //view payload
      view: initianlFormView,
    });
  } catch (e) {
    logger.error(e);
  }
});

app.action('users_select-action', async ({ ack, body, client, logger }) => {
  console.log('You are on users_select-action');
  console.log('users_select-action body');
  console.log(body);
  const viewId = body.view.id;
  console.log(`View Id; ${viewId}`);
  const selectedUserId = body.actions[0].selected_user;
  console.log(`Selected User Id: ${selectedUserId}`);
  const resp = await app.client.users.info({ user: selectedUserId });
  console.log(resp);

  console.log(`Selected User Name: ${resp.user.profile.real_name}`);
  const selectedUserName = resp.user.profile.real_name;

  await client.views.update({
    view_id: viewId,
    view: snowUserFoundBlock(selectedUserName),
  });

  console.log(`Getting active bridges in firebase`);
  getActiveBridges();
  await ack(); //aknowledge the command request
});

app.view('engage_name', async ({ ack, body, view, logger }) => {
  await ack();
  console.log(`You Are on the engage_name handler`);
  console.log(`users_select-action Printing Body`);
  console.log(body);
  console.log(`User Id: ${body.user.name}`);
  console.log(`User Id: ${body.user.id}`);
  console.log(`View`);
  console.log(view);
});
