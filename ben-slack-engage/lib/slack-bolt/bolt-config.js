'use strict'

const { App, AwsLambdaReceiver} = require('@slack/bolt');
const { slackConfig } = require('../../app-config');

const awsLambdaReceiver = new AwsLambdaReceiver({
    signingSecret: slackConfig.SLACK_SIGNING_SECRET
});

const app = new App({
    token: slackConfig.SLACK_BOT_TOKEN,
    receiver: awsLambdaReceiver,
});

module.exports = {
    awsLambdaReceiver,
    app
}
